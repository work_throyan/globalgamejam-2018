﻿using UnityEngine;
using System.Collections;

public class SoundDelay : MonoBehaviour
{

    public AudioSource audioSource;

    public AudioClip first;
    public AudioClip day;
    public AudioClip night;

    public SoundSettings soundSettings;

    // Use this for initialization
    void Start()
    {
        soundSettings = FindObjectOfType<SoundSettings>();
        soundSettings.SoundChanged += UnpateVolume;
        StartCoroutine(WaitAndPlay());

    }

    private void OnDestroy()
    {
        soundSettings.SoundChanged -= UnpateVolume;
    }

    private IEnumerator WaitAndPlay()
    {
        audioSource.volume = soundSettings.value;
        audioSource.clip = first;
        audioSource.Play();
        yield return new WaitForSeconds(first.length);
        PlayMainClip();
        
    }

    public void PlayMainClip()
    {
        if (!nightTheme)
        {
            audioSource.clip = day;
        }
        else
        {
            audioSource.clip = night;
        }
        audioSource.Play();
    }

    public void UnpateVolume(float val)
    {
        audioSource.volume = val;
    }

    private bool nightTheme = false;

    public void SwithTheme()
    {
        nightTheme = !nightTheme;
        PlayMainClip();
    }

}
