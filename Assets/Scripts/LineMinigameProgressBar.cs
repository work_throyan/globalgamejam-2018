﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LineMinigameProgressBar : MonoBehaviour
{
	public LineMinigameBorder border;
	
	public int currentScore, maxScore;

	public Image image;

	public UnityEvent onWin;
	public GameObject EndLevelPanel;
	
	private void Start()
	{
		border.onCatchGood.AddListener(() => { currentScore++; OnScoreUpdated(); });
		border.onCatchBad.AddListener(() => { currentScore--; OnScoreUpdated(); });
		currentScore = maxScore / 2;
		OnScoreUpdated();
	}

	private void OnScoreUpdated()
	{
		if (currentScore < 0)
		{
			Time.timeScale = 0;
			EndLevelPanel.SetActive(true);
		}

		if (currentScore >= maxScore)
		{
			onWin.Invoke();
		}
		
		image.fillAmount =  currentScore / (float)maxScore;
	}
}