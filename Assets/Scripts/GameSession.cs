﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSession : MonoBehaviour {

	public int downgradeLimit = 2;
	public GameObject m_EndLevelPanel;
	public GameObject winPanel;

	protected CarInputController carInputController;
	protected Downgrade[] downgrades = new Downgrade[0];
	protected string m_endLevelPanelName;
	protected string winPanelName;

	protected void Awake() {
		if (FindObjectsOfType<GameSession>().Length > 1) {
			Destroy(this);

			return;
		}

		DontDestroyOnLoad(this);
		SceneManager.sceneLoaded += ApplyDowngrades;
		m_endLevelPanelName = m_EndLevelPanel.name;
		winPanelName = winPanel.name;
	}

	protected void OnDestroy() {
		SceneManager.sceneLoaded -= ApplyDowngrades;
	}

	public void AddDowngrade(Downgrade downgrade) {
		var type = downgrade.type;
		var power = downgrade.power;
		downgrade.gameObject.SetActive(false);
		var myDowngrade = gameObject.AddComponent<Downgrade>();
		myDowngrade.type = type;
		myDowngrade.power = power;
		var newDowngrades = new Downgrade[downgrades.Length + 1];
		downgrades.CopyTo(newDowngrades, 0);
		newDowngrades[downgrades.Length] = myDowngrade;
		downgrades = newDowngrades;
	}

	public void EndLevel() {
		carInputController.Pause();
		if (downgrades.Length < downgradeLimit) {
			m_EndLevelPanel.SetActive(true);

			return;
		}

		winPanel.SetActive(true);

		Debug.Log("Bye bye");
		Destroy(gameObject);
	}

	protected void ApplyDowngrades(Scene scene, LoadSceneMode mode) {
		if (carInputController == null) {
			carInputController = FindObjectOfType<CarInputController>();
		}
		if (m_EndLevelPanel == null) {
			m_EndLevelPanel = GameObject.Find(m_endLevelPanelName);
		}
		if (winPanel == null) {
			winPanel = GameObject.Find(winPanelName);
		}
		m_EndLevelPanel.SetActive(false);
		winPanel.SetActive(false);

		foreach (var downgrade in downgrades) {
			downgrade.Apply();
		}
	}
}
