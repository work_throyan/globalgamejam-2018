﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class Comix : MonoBehaviour {
	public Image comixImage;
	public GameObject skipButton;
	public Sprite[] comics;
	public GameObject m_Logo;
	int i = 0;






	private void Start() {
		StartCoroutine(ViewLogo());
	}

	IEnumerator ViewLogo() {
		m_Logo.SetActive(true);
		yield return new WaitForSeconds(1);
		m_Logo.SetActive(false);
	}
	public void NextSlide() {
		Debug.Log("Click");
		if (i < comics.Length) {
			comixImage.sprite = comics[i];
			if (i == 0)
				FindObjectOfType<SoundSettings>().SpawnSound(7);
			else if (i == 1)
				FindObjectOfType<SoundSettings>().SpawnSound(5);
			else if (i == 2)
				FindObjectOfType<SoundSettings>().SpawnSound(6);
		} else {
			SceneManager.LoadScene("Menu");
		}
		i++;
	}

}
