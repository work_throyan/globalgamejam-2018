﻿using Assets.Scripts.Settings;
using Assets.Scripts.UI.Localization;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Assets.Scripts
{
    public class FileIO : MonoBehaviour
    {

        protected const string GAMEDATA_DIR_NAME = "GameData";
        protected const string LOCALIZATION_DIR_NAME = "Localization";
        
        protected string gameDataDirPath;
        protected string localizationDirPath;

        protected void Awake()
        {
            gameDataDirPath = string.Format("{0}/{1}", Application.persistentDataPath, GAMEDATA_DIR_NAME);
            Directory.CreateDirectory(gameDataDirPath);
            localizationDirPath = string.Format("{0}/{1}", gameDataDirPath, LOCALIZATION_DIR_NAME);
            Directory.CreateDirectory(localizationDirPath);
            GenerateTestLocalizationFiles();
        }

        /// <summary>
        /// Generate test localizations
        /// </summary>
        private void GenerateTestLocalizationFiles()
        {
            LocalizationTemplate localizationTemplateEn = new LocalizationTemplate
            {
                localizationStrings = new LocalizationString[]
                {
						new LocalizationString("Start", "Start"),
					new LocalizationString("Settings", "Settings"),
					new LocalizationString("About us", "About us"),
					new LocalizationString("Language", "Language"),
					new LocalizationString("Sound", "Sound"),
					new LocalizationString("Save your souls!", "Save your souls!"),
					new LocalizationString("This guys:", "This guys:"),
					new LocalizationString("Skip Story", "Next Slide"),
					new LocalizationString("Searching prisoned chickens", "Searching prisoned chickens"),
					new LocalizationString("Peep-peep-peep...", "Peep-peep-peep..."),
					new LocalizationString("Pee-pee-pee...", "Pee-pee-pee..."),
					new LocalizationString("Co-Co!", "Co-Co!"),
					new LocalizationString("Co-Co-Co...", "Co-Co-Co..."),
					new LocalizationString("Wee ree hee hee hee!", "Wee ree hee hee hee!"),
					new LocalizationString("Co-Co-Ro-Ko", "Co-Co-Ro-Ko"),
					new LocalizationString("Wrrrumm...", "Wrrrumm..."),
					new LocalizationString("Eee...", "Eee..."),
					new LocalizationString("Doof", "Doof"),
					new LocalizationString("Bdshch", "Bdshch"),
					new LocalizationString("Dooh-dooh-dooh", "Dooh-dooh-dooh"),
					new LocalizationString("Plup", "Plup"),
					new LocalizationString("Bulp", "Bulp"),
					new LocalizationString("Trung", "Trung"),
					new LocalizationString("Heey (", "Heey ("),
					new LocalizationString("Trung", "Trung"),
					new LocalizationString("Dzing", "Dzing"),
					new LocalizationString("Hr-sh", "Hr-sh"),
					new LocalizationString("Pooke", "Pooke"),
					new LocalizationString("Yeah!", "Yeah!"),
					new LocalizationString("Fix your Car!", "Fix your Car!"),
					new LocalizationString("Repair", "Repair"),
					new LocalizationString("Chicks on the road!", "Chicks on the road!"),
					new LocalizationString("Refuel your Car!", "Refuel your Car!"),
					new LocalizationString("Do it!", "Do it!"),
					new LocalizationString("Ok!", "Ok!"),
					new LocalizationString("Our team", "Our team"),
					new LocalizationString("Game Over: Your car is crashed", "Game Over: Your car is crashed"),
					new LocalizationString("Game Over: Out of fuel", "Game Over: Out of fuel")
				}
            };
            SaveLocalizationFile(LocatizationType.En, LocalizationTemplate.Serialize(localizationTemplateEn));

            LocalizationTemplate localizationTemplateRu = new LocalizationTemplate
            {
                localizationStrings = new LocalizationString[]
                {
					new LocalizationString("Start", "Старт"),
					new LocalizationString("Settings", "Настройки"),
					new LocalizationString("About us", "О нас"),
					new LocalizationString("Language", "Язык"),
					new LocalizationString("Sound", "Звук"),
					new LocalizationString("Save your souls!", "Бегите, глупцы!"),
					new LocalizationString("This guys:", "Вот эти ребята:"),
					new LocalizationString("Skip Story", "Пропустить историю"),
					new LocalizationString("Searching prisoned chickens", "Поиск похищеных цыплят"),
					new LocalizationString("Peep-peep-peep...", "Пиип-пиип-пиип..."),
					new LocalizationString("Pee-pee-pee...", "Пии-пии-пии..."),
					new LocalizationString("Co-Co!", "Ко-Ко!"),
					new LocalizationString("Co-Co-Co...", "Ко-Ко-Ко..."),
					new LocalizationString("Wee ree hee hee hee!", "Уии рии хи хи хи!"),
					new LocalizationString("Co-Co-Ro-Ko", "Ко-Ко-Ро-Ко!"),
					new LocalizationString("Wrrrumm...", "Врррум..."),
					new LocalizationString("Eee...", "Иии..."),
					new LocalizationString("Doof", "Дуфф"),
					new LocalizationString("Bdshch", "Бдыщ"),
					new LocalizationString("Dooh-dooh-dooh", "Дух-дух-дух"),
					new LocalizationString("Plup", "Чвяк"),
					new LocalizationString("Bulp", "Бульк"),
					new LocalizationString("Trung", "Трынь"),
					new LocalizationString("Heey (", "Эээй ("),
					new LocalizationString("Trung", "Трынь"),
					new LocalizationString("Dzing", "Дзынь"),
					new LocalizationString("Hr-sh", "Хршщ"),
					new LocalizationString("Pooke", "Пук"),
					new LocalizationString("Yeah!", "Ура!"),
					new LocalizationString("Fix your Car!", "Почини машину!"),
					new LocalizationString("Repair", "Починить"),
					new LocalizationString("Chicks on the road!", "Цыпочки в тачке, погнали!"),
					new LocalizationString("Refuel your Car!", "Заправь машину!"),
					new LocalizationString("Do it!", "Заправить!"),
					new LocalizationString("Ok!", "Ок!"),
					new LocalizationString("Our team", "Наша команда"),
					new LocalizationString("Game Over: Your car is crashed", "Игра окончена: Машина разбилась"),
					new LocalizationString("Game Over: Out of fuel", "Игра окончена: закончилось топливо")
				}
            };
            SaveLocalizationFile(LocatizationType.Ru, LocalizationTemplate.Serialize(localizationTemplateRu));
				

            LocalizationTemplate localizationTemplateUa = new LocalizationTemplate
            {
                localizationStrings = new LocalizationString[]
                {
						new LocalizationString("Start", "Гайда"),
					new LocalizationString("Settings", "Налаштування"),
					new LocalizationString("About us", "Наші хлопці"),
					new LocalizationString("Language", "Мова"),
					new LocalizationString("Sound", "Звук"),
					new LocalizationString("Save your souls!", "Свободу пташеням!"),
					new LocalizationString("This guys:", "Ось ці файні хлопці:"),
					new LocalizationString("Skip Story", "До біса це"),
					new LocalizationString("Searching prisoned chickens", "Пошук захоплених ципочок"),
					new LocalizationString("Peep-peep-peep...", "Пііп-пііп-пііп..."),
					new LocalizationString("Pee-pee-pee...", "Піі-піі-піі..."),
					new LocalizationString("Co-Co!", "Ко-Ко!"),
					new LocalizationString("Co-Co-Co...", "Ко-Ко-Ко..."),
					new LocalizationString("Wee ree hee hee hee!", "Уии рии хи хи хи!"),
					new LocalizationString("Co-Co-Ro-Ko", "Ко-Ко-Ро-Ко!"),
					new LocalizationString("Wrrrumm...", "Врррум..."),
					new LocalizationString("Eee...", "Ііі..."),
					new LocalizationString("Doof", "Дуфф"),
					new LocalizationString("Bdshch", "Бдищ"),
					new LocalizationString("Dooh-dooh-dooh", "Дух-дух-дух"),
					new LocalizationString("Plup", "Чвак"),
					new LocalizationString("Bulp", "Бульк"),
					new LocalizationString("Trung", "Трынь"),
					new LocalizationString("Heey (", "Ееей ("),
					new LocalizationString("Trung", "Тринь"),
					new LocalizationString("Dzing", "Дзинь"),
					new LocalizationString("Hr-sh", "Хршщ"),
					new LocalizationString("Pooke", "Пук"),
					new LocalizationString("Yeah!", "Гайда!"),
					new LocalizationString("Fix your Car!", "Тачило вхламину!"),
					new LocalizationString("Repair", "Відремонтувати"),
					new LocalizationString("Chicks on the road!", "Цiпочки у тачилi, айда!"),
					new LocalizationString("Refuel your Car!", "Додати пального!"),
					new LocalizationString("Do it!", "Знайти!"),
					new LocalizationString("Ok!", "Ок!"),
					new LocalizationString("Our team", "Наша команда"),
					new LocalizationString("Game Over: Your car is crashed", "Ви продеребенили грати. Що ж зламалося?"),
					new LocalizationString("Game Over: Out of fuel", "Ви продеребенили грати: Немає палива")
				}
            };
            SaveLocalizationFile(LocatizationType.Ua, LocalizationTemplate.Serialize(localizationTemplateUa));
        }

        /// <summary>
        /// Read text from text file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string ReadTextFile(string filePath)
        {
            string fileText = File.ReadAllText(filePath);
            return fileText;
        }
					

        /// <summary>
        /// Write text to text file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="content"></param>
        public void WriteTextFile(string filePath, string content)
        {
            File.WriteAllText(filePath, content);
        }
				

        /// <summary>
        /// Read localization file
        /// </summary>
        /// <param name="locatizationType"></param>
        /// <returns></returns>
        public string ReadLocalizationFile(LocatizationType locatizationType)
        {
            string filePath = string.Format("{0}/{1}.json", localizationDirPath, locatizationType.ToString());
            return ReadTextFile(filePath);
        }

        /// <summary>
        /// Write localization file
        /// </summary>
        /// <param name="locatizationType"></param>
        public void SaveLocalizationFile(LocatizationType locatizationType, string serializadLocalization)
        {
            string filePath = string.Format("{0}/{1}.json", localizationDirPath, locatizationType.ToString());
            WriteTextFile(filePath, serializadLocalization);
            Debug.LogFormat("Save file {0}", filePath);
        }
    }
}
