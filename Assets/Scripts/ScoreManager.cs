using UnityEngine;

public class ScoreManager : MonoBehaviour {
	public float Score { get { return score; } }
	public float dayScorePerSecond = 1f;
	public float nightScorePerSeconds = 3f;

	public CarInputController inputController;

	protected float score = 0f;

	protected void Update() {
		if (inputController.IsLightEnabled) {
			score += dayScorePerSecond * Time.deltaTime;
		} else {
			score += nightScorePerSeconds * Time.deltaTime;
		}
	}

	protected void OnValidate() {
		if (inputController == null) {
			inputController = FindObjectOfType<CarInputController>();
		}
	}
}