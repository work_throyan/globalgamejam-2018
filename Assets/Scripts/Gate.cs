﻿using UnityStandardAssets.Vehicles.Car;
using UnityEngine;

public class Gate : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.GetComponent<CarController>() != null)
        {
            FindObjectOfType<GameSession>().EndLevel();
        }
    }
}