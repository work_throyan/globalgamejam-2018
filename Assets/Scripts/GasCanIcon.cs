﻿using UnityEngine;
using UnityEngine.UI;

public class GasCanIcon : MonoBehaviour
{
    public CarGasCan carGasCan;

    private Image image;

    private float maxCapacity;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    private void Start()
    {
        maxCapacity = carGasCan.capacity;
    }

    private void Update()
    {
        image.fillAmount = carGasCan.capacity / maxCapacity;
    }
}