﻿using UnityEngine;
using System;

[Serializable]
public class ObstacleSpawnData
{
    public Obstacle obstaclePrefab;

    public int relativeFrequency;

    [NonSerialized] public RangeInt spawnChance;
}