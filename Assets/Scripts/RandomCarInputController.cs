﻿using System.Collections;
using UnityEngine;
using UnityStandardAssets.Vehicles.Car;
using Random = UnityEngine.Random;

public class RandomCarInputController : MonoBehaviour
{
	private CarController carController;

	[Header("X: min, Y: max")]
	public Vector2 turnSeconds;

	[Range(-1, 1)]
	public float gas;

	[Header("X: min, Y: max")]
	public Vector2 turnPowerRange;

	public float turnPowerSmooth;

	public float externalForceSmooth;
	
	[Header("[DEBUG] X: current, Y: target")]
	public Vector2 turnPower;

	public Vector3 externalForce;

	private bool flag;

	private void Awake()
	{
		carController = GetComponent<CarController>();
	}

	protected void OnEnable() {
		controlBlocked = false;
		canBePunched = true;
		// replace coroutine with update
		StartRandomMoveCoroutine();
	}

	private void Update()
	{
		var externalForceTouch = externalForce * externalForceSmooth * Time.deltaTime;
		externalForce -= externalForceTouch;
		transform.Translate(externalForceTouch.x, externalForceTouch.z, externalForceTouch.y);
		
		//

		turnPower.x += (turnPower.y - turnPower.x) * turnPowerSmooth * Time.deltaTime;
		
		//
		
		carController.Move(turnPower.x, gas, gas, 0F);
	}

	private void StartRandomMoveCoroutine()
	{
		StartCoroutine(MoveCoroutine(Random.Range(turnSeconds.y, turnSeconds.y)));
	}

	private IEnumerator MoveCoroutine(float seconds)
	{
		while (controlBlocked)
		{
			yield return null;
		}
		turnPower.y = Random.Range(turnPowerRange.x, turnPowerRange.y) * (flag ? 1 : -1);
		flag = !flag;
		yield return new WaitForSeconds(seconds);
		StartRandomMoveCoroutine();
	}

	private bool canBePunched = true;
	
	public void Punch(Vector2 vector)
	{
		if (canBePunched)
		{
			canBePunched = false;
			StartCoroutine(PunchAfterTimeout(vector, 0.2F));
		}
	}

	private bool controlBlocked = false;

	private IEnumerator PunchAfterTimeout(Vector2 vector, float timeoutInSeconds)
	{
		yield return new WaitForSeconds(timeoutInSeconds);
		externalForce += new Vector3(vector.x, vector.y, 0);
		turnPower.y = (turnPower.y + (vector.x * 10F)) / 2F;
		turnPower.x = turnPower.y;
		controlBlocked = true;
        FindObjectOfType<SoundSettings>().SpawnSound(3);
		yield return new WaitForSeconds(timeoutInSeconds);
		controlBlocked = false;
		canBePunched = true;
	}
}