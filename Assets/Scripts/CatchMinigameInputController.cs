﻿using System.Linq;
using UnityEngine;

public class CatchMinigameInputController : MonoBehaviour
{
	public CatchMinigamePlatform platform;

	private float targetX;

	public float speed;

	private void Update()
	{
		{
			if (Input.GetMouseButton(0))
			{
				UpdateInput(Input.mousePosition);
			}
		}
		
		if (Mathf.Max(targetX, platform.transform.position.x) - Mathf.Min(targetX, platform.transform.position.x) > Mathf.Abs(speed))
		{
			if (targetX < platform.transform.position.x)
			{
				platform.transform.Translate(-speed, 0, 0);
			}
			else if (targetX > platform.transform.position.x)
			{
				platform.transform.Translate(speed, 0, 0);
			}
		}
	}

	private void UpdateInput(Vector2 position)
	{ 
		var v3 = Camera.main.ScreenToWorldPoint(new Vector3(position.x, position.y, 10F));
		targetX = v3.x;
		
		Debug.Log("targetX: " + targetX + " posX: " + platform.transform.position.x);
	}
}