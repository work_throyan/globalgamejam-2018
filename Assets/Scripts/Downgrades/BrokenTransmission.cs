﻿using UnityEngine;
using UnityStandardAssets.Vehicles.Car;

[RequireComponent(typeof(CarController))]
public class BrokenTransmission : MonoBehaviour
{
	public float brakeForce = 1f;
	public Vector2 brakeTime = new Vector2(1f, 5f);

	protected CarController carController;
	protected float nextBrakeTime;

	protected void Awake()
	{
		carController = GetComponent<CarController>();
	}

	protected void Start() {
		nextBrakeTime = Time.time + Random.Range(brakeTime.x, brakeTime.y);
	}

	protected void Update() {
		if (Time.time < nextBrakeTime) {
			return;
		}

		nextBrakeTime = Time.time + Random.Range(brakeTime.x, brakeTime.y);
		carController.Move(0f, 0f, -brakeForce, 0f);
	}
}