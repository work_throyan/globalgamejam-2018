using UnityEngine;
using UnityEngine.UI;

public enum DowngradeType {
	LessFuel = 0,
	BrokenTransmission = 1,
	WeakChicken = 2,
	SlowSpeed = 3,
}

public class Downgrade : MonoBehaviour {
	public DowngradeType type;

	public float power;

	protected void Start() {
		foreach (var downgrade in FindObjectsOfType<Downgrade>()) {
			if (downgrade.type == type && downgrade != this) {
				Destroy(this);
				GetComponent<Button>().interactable = false;

				break;
			}
		}
	}

	public void Apply() {
		switch (type) {
			case DowngradeType.LessFuel:
				var can = Object.FindObjectOfType<CarGasCan>();
				can.gasPer100KM += power;
				can.Refresh();
				break;
			case DowngradeType.BrokenTransmission:
				var brokenTransmission = Object.FindObjectOfType<RandomCarInputController>().gameObject.AddComponent<BrokenTransmission>();
				brokenTransmission.brakeForce = power;
				break;
			case DowngradeType.WeakChicken:
				Object.FindObjectOfType<CarInputController>().forceMultipiller += power;
				break;
			case DowngradeType.SlowSpeed:
				Object.FindObjectOfType<RandomCarInputController>().gas -= power;
				break;
		}
	}

	public void PickUp() {
		FindObjectOfType<GameSession>().AddDowngrade(this);
	}
}