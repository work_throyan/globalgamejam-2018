﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class CatchMinigameProgressBar : MonoBehaviour 
{
	public CatchMinigamePlatform platform;
	public GameObject EndLevelPanel;
	public int currentScore, maxScore;

	public Image image;

	public UnityEvent onWin;

	
	private void Start()
	{
		platform.onCatchGood.AddListener(() => { currentScore++; OnScoreUpdated(); });
		platform.onCatchBad.AddListener(() => { currentScore--; OnScoreUpdated(); });
		currentScore = maxScore / 2;
		OnScoreUpdated();
	}

	private void OnScoreUpdated()
	{
		if (currentScore < 0)
		{
			Time.timeScale = 0;
			EndLevelPanel.SetActive(true);
		}

		if (currentScore >= maxScore)
		{
			onWin.Invoke();
		}

		image.fillAmount = currentScore / (float) maxScore;
	}
}