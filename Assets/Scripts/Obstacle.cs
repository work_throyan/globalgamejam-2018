﻿using UnityEngine.Events;
using UnityStandardAssets.Vehicles.Car;
using UnityEngine;

public class Obstacle : MonoBehaviour {
	public MapObjectsGenerator.MapObjectType m_Type;
	public bool isDestroyable;
	public UnityEvent onHitWithCar = new UnityEvent();

	protected Transform originParent;
	protected GameObject root;

	void Start() {
		originParent = transform.parent;
	}

	private void OnCollisionEnter(Collision other) {
		if (other.gameObject.GetComponent<CarController>() == null) {
			return;
		}

		if (m_Type == MapObjectsGenerator.MapObjectType.Gas) {
			if (CarHit.e_CarHitGasDelegate != null) {
				CarHit.e_CarHitGasDelegate();
			}
		} else if (m_Type == MapObjectsGenerator.MapObjectType.Gear) {
			if (CarHit.e_CarHitGearDelegate != null) {
				CarHit.e_CarHitGearDelegate();
			}
		} else if (CarHit.e_CarHitObstacleDelegate != null) {
			CarHit.e_CarHitObstacleDelegate(1);
			FindObjectOfType<SoundSettings>().SpawnSound(2);
		}

		if (Vector3.Dot(
				other.contacts[0].normal,
				other.gameObject.GetComponent<Rigidbody>().velocity.normalized
			) < -0.2f) {
			return;
		}

		other.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;



		if (isDestroyable) {
			gameObject.SetActive(false);
			return;
		}

		other.transform.Rotate(Vector3.up, 180f);
		other.transform.Translate(0, 0, 0.3f);
	}
}