﻿using System.Collections;
using UnityEngine;

public class GameMode : MonoBehaviour {
	public CarInputController inputController;
	public float dayLength = 20f;
	public float nightLength = 7f;

	protected void Start() {
		StartCoroutine(SwitchDayTime());
	}

	protected void OnValidate() {
		if (inputController == null) {
			inputController = FindObjectOfType<CarInputController>();
		}
	}

	protected IEnumerator SwitchDayTime() {
		while (true) {
			float time = inputController.IsLightEnabled ? dayLength : nightLength;
			yield return new WaitForSeconds(time);
			inputController.ToggleLight(!inputController.IsLightEnabled);
		}
	}
}
