﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//public enum MapObjectType { Tree, Stone, Pig, Gas, Gear }
public class MapObjectPool : MonoBehaviour {

	public Obstacles[] m_Obstacles;
	public Transform m_PoolParent;



	void Awake() {
		for (int i = 0; i < m_Obstacles.Length; i++) {
			m_Obstacles[i].m_PooledObjects = new Queue<MapObject>();
			for (int j = 0; j < m_Obstacles[i].m_ObstaclesCount; j++) {

				MapObject mapObject = new MapObject(Instantiate(m_Obstacles[i].m_ObstaclePrefab, m_PoolParent), m_Obstacles[i].m_Type, true);
				if (m_Obstacles[i].m_Type == MapObjectsGenerator.MapObjectType.Gas) {
					OffscreenTargetIndicator.m_GasCanList.Add(mapObject);
				}
				if (m_Obstacles[i].m_Type == MapObjectsGenerator.MapObjectType.Gear) {
					OffscreenTargetIndicator.m_GearList.Add(mapObject);
				}
				m_Obstacles[i].m_PooledObjects.Enqueue(mapObject);
			}
		}
	}

	public MapObject GetObject(MapObjectsGenerator.MapObjectType type) {
		for (int i = 0; i < m_Obstacles.Length; i++) {
			if (m_Obstacles[i].m_Type == type) {
				var obj = m_Obstacles[i].m_PooledObjects.Dequeue();
				obj.m_Instance.SetActive(true);
				obj.m_IsPooled = false;
				return obj;
			}
		}
		return null;
	}
	public void PoolObject(MapObject mapObject) {
		mapObject.m_Instance.SetActive(false);
		mapObject.m_IsPooled = true;
		for (int i = 0; i < m_Obstacles.Length; i++) {
			if (m_Obstacles[i].m_Type == mapObject.m_Type) {
				m_Obstacles[i].m_PooledObjects.Enqueue(mapObject);
				break;
			}
		}
	}
}
[System.Serializable]
public struct Obstacles {
	public MapObjectsGenerator.MapObjectType m_Type;
	public GameObject m_ObstaclePrefab;
	public int m_ObstaclesCount;
	public Queue<MapObject> m_PooledObjects;
}
