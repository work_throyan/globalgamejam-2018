﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {
	public enum CompassDirection { N, NE, E, SE, S, SW, W, NW }
	public GameObject m_TilePrefab;
	public Transform m_TilesParent;
	public Transform m_Car;
	public TilePool m_TilePool;
	public MapObjectsGenerator m_MapObjectsGenerator;
	MapTile[,] m_Tiles;
	int m_currentTileX;
	int m_currentTileY;
	public float m_TileZ;
	float m_tileScale;
	

	public void Start() {
		m_Tiles = new MapTile[3, 3];
		m_tileScale = m_TilePrefab.transform.localScale.x;
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				MapTile tile = new MapTile(Instantiate(m_TilePrefab, m_TilesParent));
				tile.m_Instance.SetActive(true);
				m_Tiles[i, j] = tile;
			}
		}

		m_Tiles[0, 0].m_Transform.localPosition = new Vector3(-m_tileScale, -m_tileScale, m_TileZ);
		m_Tiles[1, 0].m_Transform.localPosition = new Vector3(0, -m_tileScale, m_TileZ);
		m_Tiles[2, 0].m_Transform.localPosition = new Vector3(m_tileScale, -m_tileScale, m_TileZ);

		m_Tiles[0, 1].m_Transform.localPosition = new Vector3(-m_tileScale, 0, m_TileZ);
		m_Tiles[1, 1].m_Transform.localPosition = new Vector3(0, 0, m_TileZ);
		m_Tiles[2, 1].m_Transform.localPosition = new Vector3(m_tileScale, 0, m_TileZ);

		m_Tiles[0, 2].m_Transform.localPosition = new Vector3(-m_tileScale, m_tileScale, m_TileZ);
		m_Tiles[1, 2].m_Transform.localPosition = new Vector3(0, m_tileScale, m_TileZ);
		m_Tiles[2, 2].m_Transform.localPosition = new Vector3(m_tileScale, m_tileScale, m_TileZ);

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				if (i == 1 && j == 1) {
					m_Tiles[i, j].m_Map = m_MapObjectsGenerator.GenerateEmptyMap(m_Tiles[i, j].m_Transform);
					continue;
				} else {
					m_Tiles[i, j].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[i, j].m_Transform);
				}
			}
		}
	}
	private void Update() {
		if (m_Car.position.y > m_Tiles[1, 2].m_Transform.position.y) {
			ShiftTilesPosition(CompassDirection.N);
		} else if (m_Car.position.y < m_Tiles[1, 0].m_Transform.position.y) {
			ShiftTilesPosition(CompassDirection.S);
		} else if (m_Car.position.x < m_Tiles[0, 1].m_Transform.position.x) {
			ShiftTilesPosition(CompassDirection.W);
		} else if (m_Car.position.x > m_Tiles[2, 1].m_Transform.position.x) {
			ShiftTilesPosition(CompassDirection.E);
		}
	}
	void ShiftTilesPosition(CompassDirection move) {
		switch (move) {
			case CompassDirection.N:
				m_TilePool.PoolTile(m_Tiles[0, 0]);
				m_TilePool.PoolTile(m_Tiles[1, 0]);
				m_TilePool.PoolTile(m_Tiles[2, 0]);
				m_Tiles[0, 0] = m_Tiles[0, 1];
				m_Tiles[1, 0] = m_Tiles[1, 1];
				m_Tiles[2, 0] = m_Tiles[2, 1];

				m_Tiles[0, 1] = m_Tiles[0, 2];
				m_Tiles[1, 1] = m_Tiles[1, 2];
				m_Tiles[2, 1] = m_Tiles[2, 2];

				m_Tiles[0, 2] = m_TilePool.GetTile();
				m_Tiles[1, 2] = m_TilePool.GetTile();
				m_Tiles[2, 2] = m_TilePool.GetTile();

				m_Tiles[0, 2].m_Transform.localPosition = new Vector3(m_Tiles[0, 1].m_Transform.localPosition.x, m_Tiles[0, 1].m_Transform.localPosition.y + m_tileScale, m_TileZ);
				m_Tiles[1, 2].m_Transform.localPosition = new Vector3(m_Tiles[1, 1].m_Transform.localPosition.x, m_Tiles[1, 1].m_Transform.localPosition.y + m_tileScale, m_TileZ);
				m_Tiles[2, 2].m_Transform.localPosition = new Vector3(m_Tiles[2, 1].m_Transform.localPosition.x, m_Tiles[2, 1].m_Transform.localPosition.y + m_tileScale, m_TileZ);

				m_Tiles[0, 2].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[0, 2].m_Transform);
				m_Tiles[1, 2].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[1, 2].m_Transform);
				m_Tiles[2, 2].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[2, 2].m_Transform);


				break;
			case CompassDirection.S:
				m_TilePool.PoolTile(m_Tiles[0, 2]);
				m_TilePool.PoolTile(m_Tiles[1, 2]);
				m_TilePool.PoolTile(m_Tiles[2, 2]);

				m_Tiles[0, 2] = m_Tiles[0, 1];
				m_Tiles[1, 2] = m_Tiles[1, 1];
				m_Tiles[2, 2] = m_Tiles[2, 1];

				m_Tiles[0, 1] = m_Tiles[0, 0];
				m_Tiles[1, 1] = m_Tiles[1, 0];
				m_Tiles[2, 1] = m_Tiles[2, 0];

				m_Tiles[0, 0] = m_TilePool.GetTile();
				m_Tiles[1, 0] = m_TilePool.GetTile();
				m_Tiles[2, 0] = m_TilePool.GetTile();

				m_Tiles[0, 0].m_Transform.localPosition = new Vector3(m_Tiles[0, 1].m_Transform.localPosition.x, m_Tiles[0, 1].m_Transform.localPosition.y - m_tileScale, m_TileZ);
				m_Tiles[1, 0].m_Transform.localPosition = new Vector3(m_Tiles[1, 1].m_Transform.localPosition.x, m_Tiles[1, 1].m_Transform.localPosition.y - m_tileScale, m_TileZ);
				m_Tiles[2, 0].m_Transform.localPosition = new Vector3(m_Tiles[2, 1].m_Transform.localPosition.x, m_Tiles[2, 1].m_Transform.localPosition.y - m_tileScale, m_TileZ);

				m_Tiles[0, 0].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[0, 0].m_Transform);
				m_Tiles[1, 0].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[1, 0].m_Transform);
				m_Tiles[2, 0].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[2, 0].m_Transform);
				break;
			case CompassDirection.W:
				m_TilePool.PoolTile(m_Tiles[2, 2]);
				m_TilePool.PoolTile(m_Tiles[2, 1]);
				m_TilePool.PoolTile(m_Tiles[2, 0]);

				m_Tiles[2, 2] = m_Tiles[1, 2];
				m_Tiles[2, 1] = m_Tiles[1, 1];
				m_Tiles[2, 0] = m_Tiles[1, 0];

				m_Tiles[1, 2] = m_Tiles[0, 2];
				m_Tiles[1, 1] = m_Tiles[0, 1];
				m_Tiles[1, 0] = m_Tiles[0, 0];

				m_Tiles[0, 2] = m_TilePool.GetTile();
				m_Tiles[0, 1] = m_TilePool.GetTile();
				m_Tiles[0, 0] = m_TilePool.GetTile();

				m_Tiles[0, 2].m_Transform.localPosition = new Vector3(m_Tiles[1, 2].m_Transform.localPosition.x - m_tileScale, m_Tiles[1, 2].m_Transform.localPosition.y, m_TileZ);
				m_Tiles[0, 1].m_Transform.localPosition = new Vector3(m_Tiles[1, 1].m_Transform.localPosition.x - m_tileScale, m_Tiles[1, 1].m_Transform.localPosition.y, m_TileZ);
				m_Tiles[0, 0].m_Transform.localPosition = new Vector3(m_Tiles[1, 0].m_Transform.localPosition.x - m_tileScale, m_Tiles[1, 0].m_Transform.localPosition.y, m_TileZ);

				m_Tiles[0, 2].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[0, 2].m_Transform);
				m_Tiles[0, 1].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[0, 1].m_Transform);
				m_Tiles[0, 0].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[0, 0].m_Transform);
				break;
			case CompassDirection.E:
				m_TilePool.PoolTile(m_Tiles[0, 2]);
				m_TilePool.PoolTile(m_Tiles[0, 1]);
				m_TilePool.PoolTile(m_Tiles[0, 0]);

				m_Tiles[0, 2] = m_Tiles[1, 2];
				m_Tiles[0, 1] = m_Tiles[1, 1];
				m_Tiles[0, 0] = m_Tiles[1, 0];

				m_Tiles[1, 2] = m_Tiles[2, 2];
				m_Tiles[1, 1] = m_Tiles[2, 1];
				m_Tiles[1, 0] = m_Tiles[2, 0];

				m_Tiles[2, 2] = m_TilePool.GetTile();
				m_Tiles[2, 1] = m_TilePool.GetTile();
				m_Tiles[2, 0] = m_TilePool.GetTile();

				m_Tiles[2, 2].m_Transform.localPosition = new Vector3(m_Tiles[1, 2].m_Transform.localPosition.x + m_tileScale, m_Tiles[1, 2].m_Transform.localPosition.y, m_TileZ);
				m_Tiles[2, 1].m_Transform.localPosition = new Vector3(m_Tiles[1, 1].m_Transform.localPosition.x + m_tileScale, m_Tiles[1, 1].m_Transform.localPosition.y, m_TileZ);
				m_Tiles[2, 0].m_Transform.localPosition = new Vector3(m_Tiles[1, 0].m_Transform.localPosition.x + m_tileScale, m_Tiles[1, 0].m_Transform.localPosition.y, m_TileZ);

				m_Tiles[2, 2].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[2, 2].m_Transform);
				m_Tiles[2, 1].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[2, 1].m_Transform);
				m_Tiles[2, 0].m_Map = m_MapObjectsGenerator.GenerateMap(m_Tiles[2, 0].m_Transform);
				break;
		}
	}
}
