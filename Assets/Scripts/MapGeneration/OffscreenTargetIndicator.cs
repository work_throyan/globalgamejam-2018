﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffscreenTargetIndicator : MonoBehaviour {


	public static List<MapObject> m_GasCanList = new List<MapObject>();
	public static List<MapObject> m_GearList = new List<MapObject>();
	public TargetIndicatorPool m_TargetIndicatorPool;
	public Transform m_Car;




	void LateUpdate() {
		CheckTargetIndicator(m_GasCanList);
		CheckTargetIndicator(m_GearList);
	}

	public void CheckTargetIndicator(List<MapObject> mapObjectList) {
		for (int i = 0; i < mapObjectList.Count; i++) {
			float distance = Vector3.Distance(m_Car.transform.position, mapObjectList[i].m_Transform.position);
			if (mapObjectList[i].m_Instance.activeSelf && distance < 20) {
				if (mapObjectList[i].m_TargetIndicator == null) {
					mapObjectList[i].m_TargetIndicator = m_TargetIndicatorPool.GetTargetIndicator(mapObjectList[i].m_Type);
				} else {
					Transform targetTransform = mapObjectList[i].m_Transform;
					Transform indicatorTransform = mapObjectList[i].m_TargetIndicator.m_Transform;
					Vector3 v_diff = (targetTransform.position - indicatorTransform.position);
					float atan2 = Mathf.Atan2(v_diff.y, v_diff.x);
					indicatorTransform.rotation = Quaternion.Euler(0f, 0f, atan2 * Mathf.Rad2Deg - 90);
					//Move Towards the target
					indicatorTransform.position = Vector3.MoveTowards(indicatorTransform.position, targetTransform.transform.position, 1000);
					//Clamp to the screen view
					Vector3 pos = Camera.main.WorldToViewportPoint(indicatorTransform.position);
					pos.x = Mathf.Clamp01(pos.x);
					pos.y = Mathf.Clamp01(pos.y);
					indicatorTransform.position = Camera.main.ViewportToWorldPoint(pos);
				}
			} else {
				if (mapObjectList[i].m_TargetIndicator != null) {
					m_TargetIndicatorPool.PoolTargetIndicator(mapObjectList[i].m_TargetIndicator);
					mapObjectList[i].m_TargetIndicator = null;
				}
			}
		}
	}
	private void OnDestroy() {
		m_GasCanList.Clear();
		m_GearList.Clear();
	}

}
