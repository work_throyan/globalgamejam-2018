﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePool : MonoBehaviour {

	public MapObjectPool m_MapObjectPool;
	public GameObject m_TilePrefab;
	public Transform m_PoolParent;
	public int m_TileCount;
	public MapObjectsGenerator m_MapObjectsGenerator;
	Queue<MapTile> m_PooledObjects;

	void Awake() {
		m_PooledObjects = new Queue<MapTile>();
		for (int i = 0; i < m_TileCount; i++) {
			MapTile mapTile = new MapTile(Instantiate(m_TilePrefab, m_PoolParent));			
			m_PooledObjects.Enqueue(mapTile);
		}
	}

	public MapTile GetTile() {
		var tile = m_PooledObjects.Dequeue();		
		tile.m_Instance.SetActive(true);	
		return tile;
	}

	public void PoolTile(MapTile tile) {
		MapObject[,] mapObject = tile.m_Map;
	

		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10; j++) {
				if(mapObject[i,j].m_Type != MapObjectsGenerator.MapObjectType.Empty) {
					m_MapObjectPool.PoolObject(mapObject[i, j]);
				}
			}
		}
		tile.m_Instance.SetActive(false);
		m_PooledObjects.Enqueue(tile);
	}
}
