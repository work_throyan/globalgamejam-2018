﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObject {

	public MapObjectsGenerator.MapObjectType m_Type;
	public GameObject m_Instance;
	public Transform m_Transform;
	public bool m_IsPooled;
	public TargetIndicator m_TargetIndicator;

	public MapObject(GameObject _Instance, MapObjectsGenerator.MapObjectType _Type, bool _IsPooled) {
		m_Type = _Type;
		m_Instance = _Instance;
		m_Transform = _Instance.transform;
		m_TargetIndicator = null;
		m_IsPooled = _IsPooled;
	}
	public MapObject() {
		m_Type = MapObjectsGenerator.MapObjectType.Empty;	
	}
}
