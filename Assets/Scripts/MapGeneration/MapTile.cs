﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTile {

	public GameObject m_Instance;
	public Transform m_Transform;	
	public MapObject[,] m_Map;


	public MapTile(GameObject _Instance) {
		m_Instance = _Instance;
		m_Transform = _Instance.transform;
	}
}
