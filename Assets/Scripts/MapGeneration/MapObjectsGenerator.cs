﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObjectsGenerator : MonoBehaviour {
	public enum MapObjectType { Empty, Tree, Stone, Pig, Gas, Gear };
	public MapObjectPool m_MapObjectPool;
	public Transform m_Obstacles;
	public int m_Width;
	public int m_Height;
	public float m_DistanceBetweenObjects;
	public string m_Seed;
	public bool m_UseRandomSeed;
	public MapObjectsContainer[] m_MapObjectsContainer;

	public MapObject[,] GenerateEmptyMap(Transform parent) {
		MapObject[,] map = new MapObject[m_Width, m_Height];
		for (int x = 0; x < m_Width; x++) {
			for (int y = 0; y < m_Height; y++) {
				map[x, y] = new MapObject();
			}
		}
		return map;
	}
	public MapObject[,] GenerateMap(Transform parent) {
		MapObject[,] map = new MapObject[m_Width, m_Height];
		if (m_UseRandomSeed) {
			m_Seed = Random.Range(0, 6666).ToString();
		}

		System.Random pseudoRandom = new System.Random(m_Seed.GetHashCode());
		int percentSum = 0;
		int[] chanceBorders = new int[m_MapObjectsContainer.Length];


		for (int i = 0; i < m_MapObjectsContainer.Length; i++) {
			percentSum += m_MapObjectsContainer[i].m_Percent;
			chanceBorders[i] = percentSum;
		}

		for (int x = 0; x < m_Width; x++) {
			for (int y = 0; y < m_Height; y++) {
				if (x == 0 || x == m_Width - 1 || y == 0 || y == m_Height - 1) { // boarders
					map[x, y] = new MapObject();
					continue;
				} else {
					int num = pseudoRandom.Next(0, percentSum);
					for (int i = 0; i < chanceBorders.Length; i++) {
						if (num <= chanceBorders[i] && num >= chanceBorders[i] - m_MapObjectsContainer[i].m_Percent) { // mark object in array for spawning
							if (m_MapObjectsContainer[i].m_Type == MapObjectType.Empty) {
								map[x, y] = new MapObject();
							} else {
								map[x, y] = m_MapObjectPool.GetObject(m_MapObjectsContainer[i].m_Type);
							}
							break;
						}
					}
				}
			}
		}
		for (int x = 0; x < m_Width; x++) {
			for (int y = 0; y < m_Height; y++) {
				Vector3 position = new Vector3(-m_Width / 2 + x + m_DistanceBetweenObjects + parent.localPosition.x, -m_Height / 2 + y + m_DistanceBetweenObjects + parent.localPosition.y, 0);
				if (map[x, y].m_Type != MapObjectType.Empty) {
					map[x, y].m_Transform.localPosition = position;
				}
			}
		}
		return map;
	}
}
[System.Serializable]
public struct MapObjectsContainer {
	public GameObject m_Instance;
	[Range(0, 100)]
	public int m_Percent;
	public MapObjectsGenerator.MapObjectType m_Type;
}
