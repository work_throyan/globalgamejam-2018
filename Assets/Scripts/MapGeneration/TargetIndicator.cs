﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetIndicator {
	
	public MapObjectsGenerator.MapObjectType m_Type;
	public GameObject m_Instance;
	public Transform m_Transform;
	public TargetIndicator(GameObject _Instance, MapObjectsGenerator.MapObjectType _Type) {
		m_Type = _Type;
		m_Instance = _Instance;
		m_Transform = m_Instance.transform;
	}
}
