﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetIndicatorPool : MonoBehaviour {
	public int m_PoolCount;
	public GameObject m_GasCanTargetIndicatorPrefab;
	public GameObject m_GearTargetIndicatorPrefab;
	public Transform m_Parent;

	Queue<TargetIndicator> m_PooledGasCan;
	Queue<TargetIndicator> m_PooledGear;

	// Use this for initialization
	void Awake() {
		m_PooledGasCan = new Queue<TargetIndicator>();
		m_PooledGear = new Queue<TargetIndicator>();
		for (int i = 0; i < m_PoolCount; i++) {
			TargetIndicator targetIndicator = new TargetIndicator(Instantiate(m_GasCanTargetIndicatorPrefab, m_Parent), MapObjectsGenerator.MapObjectType.Gas);
			m_PooledGasCan.Enqueue(targetIndicator);
		}
		for (int i = 0; i < m_PoolCount; i++) {
			TargetIndicator targetIndicator = new TargetIndicator(Instantiate(m_GearTargetIndicatorPrefab, m_Parent), MapObjectsGenerator.MapObjectType.Gear);
			m_PooledGear.Enqueue(targetIndicator);
		}
	}

	public TargetIndicator GetTargetIndicator(MapObjectsGenerator.MapObjectType type) {
		switch (type) {
			case MapObjectsGenerator.MapObjectType.Gas:
				if (m_PooledGasCan.Count > 0) {
					var obj = m_PooledGasCan.Dequeue();
					obj.m_Instance.SetActive(true);
					return obj;
				} else {
					Debug.LogError("Pool GasCanTargetIndicator is empty");
				}
				break;
			case MapObjectsGenerator.MapObjectType.Gear:
				if (m_PooledGear.Count > 0) {
					var obj = m_PooledGear.Dequeue();
					obj.m_Instance.SetActive(true);
					return obj;
				} else {
					Debug.LogError("Pool GearTargetIndicator is empty");
				}
				break;
		}
		return null;
	}


	public void PoolTargetIndicator(TargetIndicator targetIndicator) {
		switch (targetIndicator.m_Type) {
			case MapObjectsGenerator.MapObjectType.Gas:
				if (m_PooledGasCan.Count > 0) {
					targetIndicator.m_Instance.SetActive(false);
					m_PooledGasCan.Enqueue(targetIndicator);
				}
				break;
			case MapObjectsGenerator.MapObjectType.Gear:
				if (m_PooledGear.Count > 0) {
					targetIndicator.m_Instance.SetActive(false);
					m_PooledGear.Enqueue(targetIndicator);
				}
				break;
		}
	}
}
