﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffect : MonoBehaviour
{

    public AudioSource audioSource;

    public void Init(AudioClip audioClip, float volume)
    {
        audioSource.volume = volume;
        audioSource.clip = audioClip;
        audioSource.Play();
        StartCoroutine(PlayAndDestroy());
    }

    public IEnumerator PlayAndDestroy()
    {
        yield return new WaitWhile(() => audioSource.isPlaying);
        Destroy(gameObject);
    }

	
}
