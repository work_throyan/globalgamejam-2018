﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.Vehicles.Car;

[RequireComponent(typeof(CarController))]
[RequireComponent(typeof(RandomCarInputController))]
[RequireComponent(typeof(Rigidbody))]
public class CarGasCan : MonoBehaviour {
	public float capacity = 1f;
	public float gasPer100KM = 10f;
	public float meterToPixelCoefficient = 0.5f;
	public Text text;
	public string textFormat = "{0:0.00}l";

	protected GameObject root;

	protected CarController car;
	protected RandomCarInputController carInputController;
	protected Rigidbody rgbody;
	protected float gasPer1PX;

	public GameObject minigameWindow;

	protected void Awake() {
		car = GetComponent<CarController>();
		carInputController = GetComponent<RandomCarInputController>();
		rgbody = GetComponent<Rigidbody>();

		Refresh();
	}

	protected void OnValidate() {
		Refresh();
	}

	protected void Update() {
		if (capacity <= 0) {
			return;
		}
		var distancePerSecond = car.CurrentSpeed / 3600f;
		capacity -= gasPer1PX * distancePerSecond * Time.deltaTime;
		if (capacity < 0) {
			capacity = 0;
			car.Move(0, 0, 0, 0);
			if (CarHit.e_CarHitCarDeadDelegate != null) {
				CarHit.e_CarHitCarDeadDelegate(CarHit.CauseOfDeath.OutOfFuel);
			}
			//minigameWindow.SetActive(true);
			//FindObjectOfType<CarInputController>().Pause();
		}
		// car.enabled = capacity > 0;
		carInputController.enabled = capacity > 0;
		text.text = string.Format(textFormat, capacity);
	}

	public void LoadMinigame() {
		root = GameObject.FindGameObjectWithTag("RootSceneObject");
		SceneManager.LoadScene("Scenes/CatchMinigame", LoadSceneMode.Additive);
		SceneManager.sceneUnloaded += OnSceneUnLoaded;
		root.SetActive(false);
	}

	private void OnSceneUnLoaded(Scene scene) {
		root.SetActive(true);
		SceneManager.sceneUnloaded -= OnSceneUnLoaded;
		capacity += 0.1F;
	}

	public void Refresh() {
		gasPer1PX = gasPer100KM * meterToPixelCoefficient / 100f;
	}
}