﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CarInputController : MonoBehaviour {
	public bool IsLightEnabled { get { return light != null && light.enabled; } }

	public Chicken chicken;

	public Light light;

	public Light[] headlights = new Light[0];

	public Toggle lightToggle;

	public RandomCarInputController carController;

	public SpeedometerController speedometerController;

	public float forceMultipiller = 1F;

	protected Transform originParent;
	protected GameObject root;

	public void GoButton() {
		if (!carController.isActiveAndEnabled) {
			return;
		}
		carController.Punch(speedometerController.GetValue() * forceMultipiller);
		if (chicken != null) {
			chicken.Run();
		}
	}

	public void Restart() {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}

	public void ToggleLight(bool flag) {
		light.enabled = flag;

		foreach (var headlight in headlights) {
			headlight.enabled = !light.enabled;
		}

		PlayerPrefs.SetInt("lights", flag ? 1 : 0);
		PlayerPrefs.Save();
	}

	public void RepairCar() {
		StartSceneInBackground("Scenes/LineMinigame");
	}

	public void Pause() {
		Time.timeScale = 0;
	}

	public void Unpause() {
		Time.timeScale = 1;
	}

	protected void Start() {
		Unpause();
		// if (PlayerPrefs.HasKey("lights")) {
		// 	lightToggle.isOn = PlayerPrefs.GetInt("lights") > 0;
		// }
	}

	protected void OnValidate() {
		if (light == null) {
			light = FindObjectOfType<Light>();
		}
		if (chicken == null) {
			chicken = FindObjectOfType<Chicken>();
		}
	}

	protected void StartSceneInBackground(string sceneName) {
		originParent = transform.parent;

		SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
		var scene = SceneManager.GetSceneAt(1);
		StartCoroutine(WaitForMiniGameClosed(scene));
		transform.SetParent(null);

		root = GameObject.FindGameObjectWithTag("RootSceneObject");
		root.SetActive(false);
	}

	IEnumerator WaitForMiniGameClosed(Scene scene) {
		Debug.Log(scene);
		while (!scene.isLoaded) {
			Debug.Log(scene);
			yield return null;
		}
		Debug.Log(scene);
		while (scene.isLoaded) {
			Debug.Log(scene);
			yield return null;
		}
		Debug.Log("DONE");
		root.SetActive(true);
		transform.SetParent(originParent);

		Debug.Log("scene.name  = " + scene.name);
		if (scene.name == "LineMinigame") {
			Debug.Log("win minigame");
			if (CarHit.e_CathMiniGameEnded != null) {
				CarHit.e_CathMiniGameEnded(true);
			}
		}
	}

	protected void Update() {
		if (Input.GetMouseButtonUp(0) && (EventSystem.current == null || !EventSystem.current.IsPointerOverGameObject())) {
			GoButton();
		}
	}
}
