﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CarHit : MonoBehaviour {
	public enum CauseOfDeath { OutOfFuel, CarCrash }
	public delegate void OnCarHitObstacleDelegate(int damage);
	public delegate void OnCarHitBonusDelegate();

	public delegate void OnCarDeadDelegate(CauseOfDeath causeOfDeath);

	public delegate void OnCarCathMiniGameEndDelegate(bool win);

	public static OnCarCathMiniGameEndDelegate e_CathMiniGameEnded;



	public static OnCarDeadDelegate e_CarHitCarDeadDelegate;

	public static OnCarHitObstacleDelegate e_CarHitObstacleDelegate;

	public static OnCarHitBonusDelegate e_CarHitGearDelegate;
	public static OnCarHitBonusDelegate e_CarHitGasDelegate;

	public CarInputController m_CarInputController;
	public GameObject m_RepairPanel;
	public GameObject m_GasOutPanel;
	public GameObject m_EndLevelPanel;
	public GameObject m_HPBar;
	public int m_CarHP;
	public int m_MaxCarHP;
	public Assets.Scripts.UI.Localization.LocalizationTextLoader m_EndLevelPanelText;
	string m_endLevelPanelName;
	string m_gasOutPanelName;
	string m_repairPanelName;
	string m_hPBarlName;
	public GameObject[] m_HP;
	private void Start() {
		e_CarHitObstacleDelegate += TakeDamage;
		e_CarHitGearDelegate += ShowRepairPanel;
		e_CarHitGasDelegate += ShowGasOutPanel;
		e_CarHitCarDeadDelegate += ShowEndLevelPanel;
		e_CathMiniGameEnded += RedrawHP;
		m_endLevelPanelName = m_EndLevelPanel.name;
		m_gasOutPanelName = m_GasOutPanel.name;
		m_repairPanelName = m_RepairPanel.name;


		m_hPBarlName = m_HPBar.name;
	
		

		if (m_EndLevelPanel == null) {
			m_EndLevelPanel = GameObject.Find(m_endLevelPanelName);
		}
		if (m_RepairPanel == null) {
			m_RepairPanel = GameObject.Find(m_repairPanelName);
		}
		if (m_GasOutPanel == null) {
			m_GasOutPanel = GameObject.Find(m_gasOutPanelName);
		}

	}

	void RedrawHP(bool win) {
		m_CarHP = m_MaxCarHP;
		if (m_HPBar == null) {
			m_HPBar = GameObject.Find(m_hPBarlName);
		}
		m_HPBar.GetComponent<HPBar>().RedrawHP(win, m_MaxCarHP, m_CarHP);
	}

	public void TakeDamage(int damage) {
		Debug.Log("damage = " + damage);
		if (m_CarHP - damage < 0) {
			ShowEndLevelPanel(CauseOfDeath.CarCrash);
		} else {
			m_CarHP -= damage;
			for (int i = 0; i < m_MaxCarHP; i++) {
				if (i >= m_CarHP) {
					m_HP[i].SetActive(false);
				}
			}
		}
	}
	public void ShowRepairPanel() {
		m_CarInputController.Pause();
		m_RepairPanel.SetActive(true);
	}
	public void ShowGasOutPanel() {
		m_CarInputController.Pause();
		m_GasOutPanel.SetActive(true);
	}
	public void ShowEndLevelPanel(CauseOfDeath causeOfDeath) {
		switch (causeOfDeath) {
			case CauseOfDeath.CarCrash:
				m_EndLevelPanelText.localizationStringUniqueId = "Game Over: Your car is crashed";
				break;
			case CauseOfDeath.OutOfFuel:
				m_EndLevelPanelText.localizationStringUniqueId = "Game Over: Out of fuel";
				break;
		}
		m_CarInputController.Pause();
		m_EndLevelPanel.SetActive(true);
	}
	private void OnDestroy() {
		e_CarHitObstacleDelegate -= TakeDamage;
		e_CarHitGearDelegate -= ShowRepairPanel;
		e_CarHitGasDelegate -= ShowGasOutPanel;
		e_CarHitCarDeadDelegate -= ShowEndLevelPanel;
	}
}
