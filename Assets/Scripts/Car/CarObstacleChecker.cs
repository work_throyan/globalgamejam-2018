using UnityEngine;

public class CarObstacleChecker : MonoBehaviour {

	public float distanceToNotify = 1f;
	public float maxDistanceVibrationStep = 1f;

	public LayerMask obstacleLayer;

	protected float nextVibrationTime;

#if UNITY_ANDROID || UNITY_IOS
	protected void Update() {
		if (Time.time < nextVibrationTime) {
			return;
		}

		RaycastHit hit;
		if (Physics.Raycast(transform.position, transform.forward, out hit, distanceToNotify, obstacleLayer.value)) {
			nextVibrationTime = Time.time + (hit.distance / distanceToNotify) * maxDistanceVibrationStep;
			Handheld.Vibrate();
		}
	}
#endif

	protected void OnDrawGizmosSelected() {
		Gizmos.color = Color.red;
		Gizmos.DrawLine(transform.position, transform.position + transform.forward * distanceToNotify);
	}
}