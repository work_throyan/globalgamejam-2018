﻿using UnityEngine;
using UnityEngine.Events;

public class CatchMinigamePlatform : MonoBehaviour
{
	public UnityEvent onCatchGood;
	
	public UnityEvent onCatchBad;

	private void OnTriggerEnter2D(Collider2D other)
	{
		var unit = other.GetComponent<MinigameUnit>();
		if (unit != null)
		{
			if (unit.minigameUnitType == MinigameUnitType.Good)
            {
                FindObjectOfType<SoundSettings>().SpawnSound(0);
                onCatchGood.Invoke();
			}
			else if (unit.minigameUnitType == MinigameUnitType.Bad)
            {
                FindObjectOfType<SoundSettings>().SpawnSound(1);
                onCatchBad.Invoke();
#if UNITY_ANDROID || UNITY_IOS
                Handheld.Vibrate();
#endif
			}
			Destroy(unit.gameObject);
		}
	}
}