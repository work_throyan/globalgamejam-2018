﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.UI.Localization
{
    [Serializable]
    public class LocalizationString
    {
        /// <summary>
        /// Unique string id
        /// </summary>
        public string id;
        /// <summary>
        /// Localized string
        /// </summary>
        public string stringValue;

        public LocalizationString(string id, string stringValue)
        {
            this.id = id;
            this.stringValue = stringValue;
        }
    }
}
