﻿using Assets.Scripts.Settings;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI.Localization
{
    /// <summary>
    /// Load localized text string to text component via string unique Id
    /// </summary>
    [RequireComponent(typeof(Text))]
    public class LocalizationTextLoader : MonoBehaviour
    {
        [Tooltip ("Text string unique id")]
        public string localizationStringUniqueId;
        protected LocalizationSettings localizationSettings;
        protected Text textComponent;

        protected void Awake()
        {
            textComponent = GetComponent<Text>();
            localizationSettings = FindObjectOfType<LocalizationSettings>();
            localizationSettings.LocalizationSwitched += UpdateLocalizationString;
        }

        protected void OnDestroy()
        {
            localizationSettings.LocalizationSwitched -= UpdateLocalizationString;
        }

        protected void Start()
        {
            UpdateText();
        }

        /// <summary>
        /// Update text when localization switched
        /// </summary>
        /// <param name="obj"></param>
        protected void UpdateLocalizationString(LocatizationType obj)
        {
            UpdateText();
        }
        /// <summary>
        /// Update text
        /// </summary>
        protected void UpdateText()
        {
            textComponent.text = localizationSettings.localization.FindLocalizationString(localizationStringUniqueId);
        }
    }
}
