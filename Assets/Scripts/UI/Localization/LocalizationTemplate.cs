﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.UI.Localization
{
    [Serializable]
    public class LocalizationTemplate
    {
        /// <summary>
        /// Localized strings
        /// </summary>
        public LocalizationString[] localizationStrings;
        
        /// <summary>
        /// Serialize to json
        /// </summary>
        /// <param name="localizationTemplate"></param>
        /// <returns></returns>
        public static string Serialize(LocalizationTemplate localizationTemplate)
        {
            return JsonUtility.ToJson(localizationTemplate);
        }
        /// <summary>
        /// Deserialize from json
        /// </summary>
        /// <param name="serializedObject"></param>
        /// <returns></returns>
        public static LocalizationTemplate Deserialize(string serializedObject)
        {
            return JsonUtility.FromJson<LocalizationTemplate>(serializedObject);
        }

        /// <summary>
        /// Find and return localized string
        /// </summary>
        /// <param name="stringUniqueId"></param>
        /// <returns></returns>
        public string FindLocalizationString(string stringUniqueId)
        {
            foreach (LocalizationString localizationString in localizationStrings)
            {
                if (localizationString.id == stringUniqueId)
                {
                    return localizationString.stringValue;
                }
            }
            Debug.LogWarningFormat("String \"{0}\" not found", stringUniqueId);
            return null;
        }
    }
}
