﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour {
	public Image[] m_HP;
	// Use this for initialization
	void Awake() {
		m_HP = GetComponentsInChildren<Image>();
	}

	// Update is called once per frame
	void Update() {

	}


	public void RedrawHP(bool win, int maxHp, int carHP) {
		if (m_HP[0] == null) {
			m_HP = GetComponentsInChildren<Image>();
		}
		for (int i = 0; i < maxHp; i++) {
			if (i >= carHP) {
				m_HP[i].gameObject.SetActive(false);
			} else {
				m_HP[i].gameObject.SetActive(true);
			}
		}
	}
}
