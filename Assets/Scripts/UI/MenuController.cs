﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour
{
    public GameObject settingsPanel;

    public void StartButtonClick()
    {
        SceneManager.LoadScene("MainGameplay");
    }

    public void SettingsButtonClick()
    {
        settingsPanel.SetActive(true);
    }

    public void AboutButtonClick()
    {

    }
}
