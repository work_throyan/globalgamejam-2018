using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ScoreText : MonoBehaviour {
	public string format = "{0:0}";

	public ScoreManager scoreManager;

	protected Text text;

	protected void Awake() {
		OnValidate();
		text = GetComponent<Text>();
	}

	protected void OnValidate() {
		if (scoreManager == null) {
			scoreManager = FindObjectOfType<ScoreManager>();
		}
	}

	protected void Update() {
		text.text = System.String.Format(format, scoreManager.Score);
	}
}