﻿using Assets.Scripts.Settings;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour
{
    public Text currentLanguageVal;
    private LocalizationSettings localizationSettings;
    public Slider soundSlider;

    public void Awake()
    {
        localizationSettings = FindObjectOfType<LocalizationSettings>();
        soundSlider.value = FindObjectOfType<SoundSettings>().value;
    }

    public void OnEnable()
    {
        UpdateText(localizationSettings.currentLocatizationType);
        localizationSettings.LocalizationSwitched += UpdateText;
    }

    public void OnDisable()
    {
        localizationSettings.LocalizationSwitched -= UpdateText;
    }

    public void UpdateText(LocatizationType locatizationType)
    {
        currentLanguageVal.text = locatizationType.ToString();
    }

    public void BackButtonClick()
    {
        gameObject.SetActive(false);
    }

}