﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MinigameSettings : MonoBehaviour
{
    public static MinigameSettings Instance;
    
    /**/

    public float MinUnitSpeed = 0.4F;
    
    public float MaxUnitSpeed = 1.6F;

    public float MinTimeToSpawn = 0.2f;
    
    public float MaxTimeToSpawn = 1f;

    public float BorderSizeMultiplier = 3F;

    public float ScreenWidth;

    public float HalfScreenWidth;

    public float ScreenHeight;

    public float HalfScreenHeight;
    
    /**/

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            Init();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Init()
    {
        var screenAspect = Screen.width / (float)Screen.height;         
        ScreenHeight = Camera.main.orthographicSize * 2F;
        HalfScreenHeight = ScreenHeight / 2F;
        ScreenWidth = ScreenHeight * screenAspect;
        HalfScreenWidth = ScreenWidth / 2F;
    }

    public void OnLineWin()
    {		
        SceneManager.UnloadSceneAsync("Scenes/LineMinigame");
    }

    public void OnCatchWin()
    {
        SceneManager.UnloadSceneAsync("Scenes/CatchMinigame");
    }
	public void RestarGame() {
		SceneManager.LoadScene(2);
	}
}