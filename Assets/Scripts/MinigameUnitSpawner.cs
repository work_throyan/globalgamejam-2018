﻿using UnityEngine;
using Random = UnityEngine.Random;

public class MinigameUnitSpawner : MonoBehaviour
{
	public Direction direction;

	public float timeToSpawn;

	public MinigameUnitType nextMinigameUnitType;

	public MinigameUnit[] goodPrefabs;

	public MinigameUnit[] badPrefabs;

	private void Start()
	{
		ResetTimer();
	}

	private void ResetTimer()
	{
		timeToSpawn = Time.time + Random.Range(MinigameSettings.Instance.MinTimeToSpawn,
			MinigameSettings.Instance.MaxTimeToSpawn);
	}
	
	private void Update()
	{
		if (Time.time >= timeToSpawn)
		{
			ResetTimer();
			SpawnUnit();
		}
	}

	private void SpawnUnit()
	{
		MinigameUnit minigameUnit;
		if (nextMinigameUnitType == MinigameUnitType.Good)
		{
			minigameUnit = Instantiate(goodPrefabs[Random.Range(0, goodPrefabs.Length)], transform.parent);
			minigameUnit.transform.position = transform.position;
			minigameUnit.transform.localPosition += new Vector3(Random.Range(-MinigameSettings.Instance.HalfScreenWidth, MinigameSettings.Instance.HalfScreenWidth), 0, 0);
			minigameUnit.Init(nextMinigameUnitType, direction);
			nextMinigameUnitType = MinigameUnitType.Bad;
		}
		else if (nextMinigameUnitType == MinigameUnitType.Bad)
		{
			minigameUnit = Instantiate(badPrefabs[Random.Range(0, badPrefabs.Length)], transform.parent);
			minigameUnit.transform.position = transform.position;
			minigameUnit.transform.localPosition += new Vector3(Random.Range(-MinigameSettings.Instance.HalfScreenWidth, MinigameSettings.Instance.HalfScreenWidth), 0, 0);
			minigameUnit.Init(nextMinigameUnitType, direction);
			nextMinigameUnitType = MinigameUnitType.Good;
		}
	}
}