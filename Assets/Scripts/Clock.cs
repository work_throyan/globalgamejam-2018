﻿using UnityEngine;

public class Clock : MonoBehaviour
{
    public Transform offset;
    
    public Transform arrow;
    
    private void Update()
    {
        transform.Rotate(0, 0, Time.deltaTime * 360F);
        //GetValues();
    }

    public Vector2 GetValues()
    {
        /*
        var f = offset.rotation.eulerAngles.z;
        if (f > 180F)
        {
            f = -(180F - (f - 180F));
        }
        f = -f;
        */
        
        var x = arrow.transform.rotation.eulerAngles.z;
        //
        x = Mathf.Abs(x);
        if (x > 180F)
        {
            x = 180F - (x - 180F);
        }
        x /= 180F;
        x = -1F + 2F * x;
        
        var y = transform.rotation.eulerAngles.z;
        //
        y = Mathf.Abs(y);
        if (y > 180F)
        {
            y = 180F - (y - 180F);
        }
        y /= 180F;
        //y = -1F + 2F * y;
        
        Debug.Log(new Vector2(x, y));
        
        return new Vector2(x, y);
    }
}