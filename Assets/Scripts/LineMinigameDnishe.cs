﻿using UnityEngine;
using UnityEngine.Events;

public class LineMinigameDnishe : MonoBehaviour {
	public UnityEvent onCatchGood;

	public UnityEvent onCatchBad;

	private void OnTriggerEnter2D(Collider2D other) {
		var unit = other.GetComponent<MinigameUnit>();
		if (unit != null) {
			Destroy(unit.gameObject);
		}
	}
}

