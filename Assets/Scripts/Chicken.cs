﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Chicken : MonoBehaviour
{
    public SpeedometerController speedometer;
    public Transform rotationBase;
    public float runTime = 0.4f;

    protected Animator animator;
    protected Vector3 euler;
    protected float runEndTime;

    protected void Start() {
        euler = rotationBase.localRotation.eulerAngles;
        animator = GetComponent<Animator>();
    }

    protected void Update() {
        if (Time.time < runEndTime + 0.1f) {
            return;
        }
        speedometer.enabled = true;
        // fuck it
        euler.y = -speedometer.arrowTransform.localRotation.eulerAngles.z;
        rotationBase.localRotation = Quaternion.Euler(euler);
    }

    public void Run() {
        Debug.Log("Run");
        
        if (Time.time < runEndTime) {
            return;
        }
        speedometer.enabled = false;
        animator.SetTrigger("Run");
        runEndTime = Time.time + runTime;
    }
}