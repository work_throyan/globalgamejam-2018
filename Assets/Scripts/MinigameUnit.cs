﻿using UnityEngine;

public class MinigameUnit : MonoBehaviour
{
    public MinigameUnitType minigameUnitType;

    public Direction Direction;

    public float Speed;

    public bool CanDie;

    public void Init(MinigameUnitType minigameUnitType, Direction direction)
    {
        this.minigameUnitType = minigameUnitType;
        Direction = direction;

        Speed = Random.Range(MinigameSettings.Instance.MinUnitSpeed,
            MinigameSettings.Instance.MaxUnitSpeed);

        if (direction == Direction.Down)
        {
            Speed *= -1F;
        }
    }

    private void Update()
    {
        transform.localPosition += new Vector3(0, Speed * Time.deltaTime, 0);
    }

    private void OnBecameVisible()
    {
        CanDie = true;
    }

    private void OnBecameInvisible()
    {
        if (CanDie)
        {
            Destroy(gameObject);
        }
    }
}