﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour {
	Quaternion m_StartRotation;
	Transform m_Transform;
	public float smoothSpeed = 0.125f;
	public Vector3 offset;
	public Transform target;
	Vector3 desiredPosition;
	Vector3 smoothedPosition;
	// Use this for initialization
	void Start() {
		m_Transform = transform;
		m_StartRotation = m_Transform.rotation;
	}



	void FixedUpdate() {
	//	m_Transform.rotation = m_StartRotation;
		desiredPosition = target.position + offset;
		smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
		m_Transform.position = new Vector3(smoothedPosition.x, smoothedPosition.y, -10);

		//	transform.LookAt(target);
	}

}
