﻿using Assets.Scripts.UI.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Settings
{
    public class LocalizationSettings : MonoBehaviour
    {
        public LocatizationType currentLocatizationType;
        public event Action<LocatizationType> LocalizationSwitched;
        
        protected FileIO fileIO;
        [HideInInspector]
        public LocalizationTemplate localization;

        protected LocalizationSettings instance;

        protected void Awake()
        {
            if (instance == null)
            {
                instance = this;
                fileIO = FindObjectOfType<FileIO>();
            }
            else
            {
                Destroy(gameObject);
            }
        }

        protected void Start()
        {
            if (PlayerPrefs.HasKey("lang")) {
                currentLocatizationType = (LocatizationType)PlayerPrefs.GetInt("lang");
            }
            SwitchLocalization((int)currentLocatizationType);
        }

        /// <summary>
        /// Switch localization
        /// </summary>
        /// <param name="languageEnumId"></param>
        public void SwitchLocalization(int languageEnumId)
        {
            currentLocatizationType = (LocatizationType)languageEnumId;
            PlayerPrefs.SetInt("lang", languageEnumId);
            PlayerPrefs.Save();
            localization = LocalizationTemplate.Deserialize(fileIO.ReadLocalizationFile(currentLocatizationType));
            CallLocalizationSwitchedEvent();
        }

        public void SwitchRight()
        {
            int temp = ((int)currentLocatizationType + 1) % 3;
            SwitchLocalization(temp);

        }

        public void SwitchLeft()
        {
            int temp = ((int)currentLocatizationType - 1) % 3;
            if (temp < 0) temp = 2;
            SwitchLocalization(temp);
        }

        /// <summary>
        /// Call LocalizationSwitched event
        /// </summary>
        protected void CallLocalizationSwitchedEvent()
        {
            if (LocalizationSwitched != null)
            {
                LocalizationSwitched(currentLocatizationType);
            }
        }
    }
}
