﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.Settings
{
    /// <summary>
    /// Available localization types
    /// </summary>
    public enum LocatizationType
    {
        Ru,
        Ua,
        En
    }
}
