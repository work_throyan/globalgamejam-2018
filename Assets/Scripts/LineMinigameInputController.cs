﻿using UnityEngine;

public class LineMinigameInputController : MonoBehaviour
{
    public LineMinigameBorder lineMinigameBorder;

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetMouseButtonUp(0))
        {
            lineMinigameBorder.Toggle();
        }
     
    }
}