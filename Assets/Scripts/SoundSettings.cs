﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSettings : MonoBehaviour
{
    public float value = 0.5f;

    public event Action<float> SoundChanged;

    public SoundEffect soundEffectPrefab;

    public void SetNewVal(float val)
    {
        value = val;
        if (SoundChanged != null)
        {
            SoundChanged(value);
        }
    }

    public AudioClip[] audioClips;

    public void SpawnSound(int clipId)
    {
        SoundEffect soundEffect = Instantiate(soundEffectPrefab);
        soundEffect.Init(audioClips[clipId], value);
    }
}
