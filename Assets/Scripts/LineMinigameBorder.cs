﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LineMinigameBorder : MonoBehaviour
{
    public bool active;

	public UnityEvent onCatchGood;

	public UnityEvent onCatchBad;

	public void Toggle()
    {
        active = !active;
        var scaleMul = MinigameSettings.Instance.BorderSizeMultiplier;
        if (active)
        {
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * scaleMul, transform.localScale.z * scaleMul);
        }
        else
        {
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y / scaleMul, transform.localScale.z / scaleMul);
        }
    }

    

	private void OnTriggerEnter2D(Collider2D other) {
		var unit = other.gameObject.GetComponent<MinigameUnit>();
		if (active && unit != null) {
			if (unit.minigameUnitType == MinigameUnitType.Good) {
				FindObjectOfType<SoundSettings>().SpawnSound(0);
				onCatchGood.Invoke();
			} else if (unit.minigameUnitType == MinigameUnitType.Bad) {
				FindObjectOfType<SoundSettings>().SpawnSound(1);
				onCatchBad.Invoke();
#if UNITY_ANDROID || UNITY_IOS
				Handheld.Vibrate();
#endif
			}
			Destroy(unit.gameObject);
		}
	}
}