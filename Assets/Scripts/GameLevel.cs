﻿using UnityEngine;

public class GameLevel : MonoBehaviour {

	//public Collider levelCollider;

	public Transform followTarget;
	public Vector2 followTargetScreenLocalPosition;

	public Camera camera;

	public float followSpeed = 1f;

	protected Bounds bounds;
	protected Vector2 followTargetScreenPosition;
	protected Vector2 followTargetPosition;

	protected void Awake() {
	//	bounds = levelCollider.bounds;
		OnValidate();
	}

	protected void OnValidate() {
		if (camera == null) {
			camera = FindObjectOfType<Camera>();
		}

		if (camera != null) {
			followTargetScreenPosition = new Vector2(camera.pixelRect.width, camera.pixelRect.height);
			followTargetScreenPosition.Scale(followTargetScreenLocalPosition);
			followTargetPosition = camera.ScreenToWorldPoint(followTargetScreenPosition);
		}
	}

	//protected void Update() {
	//	if (followTarget == null) {
	//		enabled = false;

	//		return;
	//	}
	//	var diff = (Vector3) followTargetPosition - followTarget.position;
	//	diff.z = 0;
	//	if (diff.magnitude < .1f) {
	//		return;
	//	}
	//	float speed = Mathf.Min(diff.magnitude, followSpeed);
	//	transform.position += diff * (speed * Time.deltaTime);
	//}
}
