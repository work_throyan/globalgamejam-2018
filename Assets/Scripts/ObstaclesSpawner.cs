﻿using System.Linq;
using UnityEngine;

public class ObstaclesSpawner : MonoBehaviour
{
	public ObstacleSpawnData[] obstacleSpawnDatas;

	private Rect spawnArea;

	public int count;

	public float spawnZ;

	private void Awake()
	{
		spawnArea = GetComponent<RectTransform>().rect;
	}

	private void Start()
	{
		if (obstacleSpawnDatas.Length < 1)
		{
			return;
		}
		var frequencySum = obstacleSpawnDatas.Select(oSd => oSd.relativeFrequency).Aggregate((a, b) => a + b);
		var current = 0;
		for (var i = 0; i < obstacleSpawnDatas.Length; i++)
		{
			var data = obstacleSpawnDatas[i];
			var next = current + data.relativeFrequency;
			data.spawnChance = new RangeInt(current, next);
			current = next;
		}
		for (var i = 0; i < count; i++)
		{
			var target = Random.Range(0, frequencySum);
			for (var i1 = 0; i1 < obstacleSpawnDatas.Length; i1++)
			{
				var data = obstacleSpawnDatas[i1];
				if (data.spawnChance.start <= target && data.spawnChance.end > target)
				{
					var obj = Instantiate(data.obstaclePrefab, transform);
					obj.transform.localPosition = new Vector3(Random.Range(spawnArea.x, spawnArea.x + spawnArea.width), Random.Range(spawnArea.y, spawnArea.y + spawnArea.height), spawnZ);
					break;
				}
			}
		}
	}
}